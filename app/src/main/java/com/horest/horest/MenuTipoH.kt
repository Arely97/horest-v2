package com.horest.horest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_dato_h.*
import kotlinx.android.synthetic.main.activity_menu_tipo_h.*

class MenuTipoH : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        var tipo = 0
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_tipo_h)
        val tvUsuario: TextView = findViewById(R.id.NameUs1)
        val usuario: String = intent.extras!!.getString("USUARIO").toString()
        tvUsuario.text = usuario
        creditos2.setOnClickListener {
            startActivity(Intent(this, Creditos::class.java))
        }
           salir1.setOnClickListener {
            if (usuario == "Google") {
                Firebase.auth.signOut()
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(this, Login::class.java))
                finish()
            }
            startActivity(Intent(this, Login::class.java))
        }

        hoteles.setOnClickListener {
            tipo = 1
            val bundle = Bundle()
            bundle.apply {
                putInt("key_tipo", tipo)
                putString("USUARIO", usuario)
            }
            val intent = Intent(this, Hotel::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
        HoRestSalon.setOnClickListener {
            tipo = 2
            val bundle = Bundle()
            bundle.apply {
                putInt("key_tipo", tipo)
                putString("USUARIO", usuario)
            }
            val intent = Intent(this, Hotel::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)

        }
        hoSalon.setOnClickListener {
            tipo = 3
            val bundle = Bundle()
            bundle.apply {
                putInt("key_tipo", tipo)
                putString("USUARIO", usuario)
            }
            val intent = Intent(this, Hotel::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)

        }

        autoh.setOnClickListener {
            tipo = 4
            val bundle = Bundle()
            bundle.apply {
                putInt("key_tipo", tipo)
                putString("USUARIO", usuario)

            }
            val intent = Intent(this, Hotel::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)

        }
    }
}