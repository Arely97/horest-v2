package com.horest.horest

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_ayuda.*
import kotlinx.android.synthetic.main.activity_ayuda.view.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlin.properties.Delegates
import kotlinx.android.synthetic.main.activity_login.btnAyuda


class Login : AppCompatActivity() {
    private val TAG = "LoginActivity"
    private var email by Delegates.notNull<String>()
    private var password by Delegates.notNull<String>()
    private lateinit var etEmail: EditText
    private lateinit var etPassword: EditText
    private lateinit var mProgressBar: ProgressDialog
    private lateinit var mAuth: FirebaseAuth
    private lateinit var databaseReference: DatabaseReference
    lateinit var mGoogleSignInClient: GoogleSignInClient
    val RC_SIGN_IN: Int = 1
    lateinit var gso: GoogleSignInOptions

    override fun onStart() {
        super.onStart()

        val user = mAuth.currentUser
        if (user != null) {
            val intent = Intent(this, Login::class.java)
            startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnlogin.setOnClickListener {
            etEmail = findViewById(R.id.etEmail)
            etPassword = findViewById(R.id.etPassword)
            mProgressBar = ProgressDialog(this)
            mAuth = FirebaseAuth.getInstance()
            email = etEmail.text.toString()
            password = etPassword.text.toString()

            if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
                if (email == "Admin" && password == "Admin") {

                    val bundle = Bundle()
                    bundle.apply {
                        putString("USUARIO", "Admin")
                    }

                    val intent = Intent(this, ViewInstructions::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)

                    Toast.makeText(this, "Bienvenido!!!", Toast.LENGTH_SHORT).show()
                } else {
                    mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this) { task ->
                            if (task.isSuccessful) {

                                val bundle = Bundle()
                                bundle.apply {
                                    putString("USUARIO", "Correo")
                                }

                                val intent = Intent(this, ViewInstructions::class.java).apply {
                                    putExtras(bundle)
                                }
                                startActivity(intent)

                                Toast.makeText(this, "Bienvenido!!!", Toast.LENGTH_SHORT).show()
                            } else {
                                Toast.makeText(
                                    this, "Error al Iniciar.",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                }
            } else {
                Toast.makeText(this, "Verifique sus datos", Toast.LENGTH_SHORT).show()
            }


        }

        create.setOnClickListener {
            startActivity(Intent(this, Registro::class.java))
        }
        btnAyuda.setOnClickListener {
            val bottomSheet = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)
            val view = LayoutInflater.from(this).inflate(R.layout.activity_ayuda, contenedor)
            view.dato.text =
                "Recuerda primero Registrar su Usuario y Contraseña o Ingresa con ' Admin ' & ' Admin '"
            bottomSheet.setContentView(view)
            bottomSheet.show()
        }
        txtForgotPassword.setOnClickListener {
            startActivity(Intent(this, ForgotPasswordActivity::class.java))
        }
        mAuth = FirebaseAuth.getInstance()

        createRequest()
        google.setOnClickListener {
            signIn();
        }

    }
    private fun createRequest() {
        // Configure Google Sign In
        gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }


    private fun signIn() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            val exception = task.exception

            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)!!
                firebaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Toast.makeText(this, "Iniciar Sesión Fallida", Toast.LENGTH_SHORT)
                    .show()
            }

        }
    }

    private fun firebaseAuthWithGoogle(account: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)

        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val bundle = Bundle()
                    bundle.apply {
                        putString("USUARIO", "Google")
                    }
                    val intent = Intent(this, ViewInstructions::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)

                    Toast.makeText(this, "Bienvenido!!!", Toast.LENGTH_SHORT).show()

                } else {
                    Toast.makeText(this, "Login Fallida ", Toast.LENGTH_SHORT).show()
                }
            }
    }
}