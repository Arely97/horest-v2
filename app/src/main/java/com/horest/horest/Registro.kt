package com.horest.horest

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_registro.*
import kotlin.properties.Delegates


class Registro : AppCompatActivity() {
    private lateinit var txtName: EditText
    private lateinit var txtLastName: EditText
    private lateinit var txtEdad: EditText
    private lateinit var txtEmail: EditText
    private lateinit var txtContra1: EditText
    private lateinit var txtContra2: EditText
    private lateinit var progressBar: ProgressDialog
    private lateinit var databaseReference: DatabaseReference
    private lateinit var database: FirebaseDatabase
    private lateinit var auth: FirebaseAuth

    //global variables
    private var firstName by Delegates.notNull<String>()
    private var lastName by Delegates.notNull<String>()
    private var edad by Delegates.notNull<String>()
    private var email by Delegates.notNull<String>()
    private var genero by Delegates.notNull<String>()
    private var password by Delegates.notNull<String>()
    private var password2 by Delegates.notNull<String>()
    private var terminos by Delegates.notNull<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)
        initialise()


    }

    private fun initialise() {
        txtName = findViewById(R.id.txtName)
        txtLastName = findViewById(R.id.txtLastName)
        txtEdad = findViewById(R.id.txtEdad)
        txtEmail = findViewById(R.id.txtEmail)
        txtContra1 = findViewById(R.id.txtContra1)
        txtContra2 = findViewById(R.id.txtContra2)
        progressBar = ProgressDialog(this)


        database = FirebaseDatabase.getInstance()
        auth = FirebaseAuth.getInstance()
        databaseReference = database.reference.child("Users")

    }

    private fun createNewAccount() {
        firstName = txtName.text.toString()
        lastName = txtLastName.text.toString()
        edad = txtEdad.text.toString()
        email = txtEmail.text.toString()
        password = txtContra1.text.toString()
        password2 = txtContra2.text.toString()
        genero = if (rdbMasculino.isChecked) "Masculino" else "Femenino"
        terminos = if (chkTerminos.isChecked) "si" else ""
        if (!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName) && !TextUtils.isEmpty(
                terminos
            )
            && !TextUtils.isEmpty(edad) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(
                password2
            )
        ) {

            if (password == password2) {
                progressBar.setMessage("Usuario registrado...")
                progressBar.show()
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) {
                        val user: FirebaseUser = auth.currentUser!!
                        verifyEmail(user);
                        val currentUserDb = databaseReference.child(user.uid)
                        currentUserDb.child("Nombre").setValue(firstName)
                        currentUserDb.child("Apellidos").setValue(lastName)
                        currentUserDb.child("Edad").setValue(edad)
                        currentUserDb.child("Contraseña").setValue(password)
                        currentUserDb.child("Genero").setValue(genero)
                        updateUserInfoAndGoHome()

                    }.addOnFailureListener {
                        Toast.makeText(
                            this, "Error en la autenticación.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
            }

            else {
                Toast.makeText(this, "Contraseñas diferentes", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Llene todos los campos", Toast.LENGTH_SHORT).show()
        }

    }

    fun credito(view: View) {
        val intent = Intent(this, Creditos::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    fun register(view: View) {
        createNewAccount()
    }

    private fun updateUserInfoAndGoHome() {
        val intent = Intent(this, Login::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)

        progressBar.hide()

    }

    private fun verifyEmail(user: FirebaseUser) {
        user.sendEmailVerification()
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Toast.makeText(
                        this,
                        "Email " + user.getEmail(),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        this,
                        "Error al verificar el correo ",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

}